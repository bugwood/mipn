//
//  SpeciDetailTableController.m
//  GeneralFrameWork
//
//  Created by Jordan Daniel on 6/7/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import "SpeciDetailTableController.h"
#import "AppDelegate.h"
#import "Speci+CoreDataProperties.h"
#import <QuartzCore/QuartzCore.h>


@interface SpeciDetailTableController ()
@end

@implementation SpeciDetailTableController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
       
    UIImageView *backgoundImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:[self.selectedSpecies indicatorThumb]]];
    [backgoundImage setAlpha:0.10];
    [self.tableView setBackgroundView:backgoundImage];
    
    [self.SciLabel setText:[self.selectedSpecies sublabel]];
    [self.comLabel setText:[self.selectedSpecies label]];
    
    NSLog(@"%@",[self.selectedSpecies descript]);
    [self.descriptWebView setNavigationDelegate:self];
    [self.descriptWebView setBackgroundColor:[UIColor clearColor]];
    [self.descriptWebView setOpaque:NO];
    [self.descriptWebView.scrollView setScrollEnabled:NO];
//    self.descriptWebView.autoresizingMask=(UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
    [self.descriptWebView loadHTMLString:[self.selectedSpecies descript] baseURL:nil];
    
    if ([[self.selectedSpecies images]count]>0) {
        NSArray *images = [[NSArray alloc]initWithArray:[self.selectedSpecies valueForKey:@"images"]];
        NSDictionary *image = [[NSDictionary alloc]initWithDictionary:[images objectAtIndex:0]];
        [self.imageSample setImage:[UIImage imageNamed:[image valueForKey:@"filename"]]];
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
//    [self willFixTextViewContent];
    [self.descriptWebView loadHTMLString:[self.selectedSpecies descript] baseURL:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:[UIColor clearColor]];
    
}

#pragma mark - Table view delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
     switch ([indexPath section]) {
        case 0:
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                return 150;
            }
            return 108;
            
        case 1:
             NSLog(@"descript height - %f",self.descriptWebView.scrollView.contentSize.height);
            return self.descriptWebView.scrollView.contentSize.height;
        
        default:
            return 100;
    }
}



#pragma mark
//-(void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation{
//    [self.activityIndicator startAnimating];
//}

-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
//    [webView sizeToFit];
    [webView evaluateJavaScript:@"document.readyState" completionHandler:^(id _Nullable complete, NSError * _Nullable error) {
        if (complete != nil) {
            [webView evaluateJavaScript:@"document.body.scrollHeight" completionHandler:^(id _Nullable height, NSError * _Nullable error) {
                for (NSLayoutConstraint *aConstraint in webView.constraints) {
                    aConstraint.constant = [(NSNumber*)height floatValue];
                }
                [self.descriptWebView setNeedsLayout];
                [self.tableView reloadData];
            }];
        }
        
    }];
    
}

-(void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Webview Error"
                                                                   message:[NSString stringWithFormat:@"Sorry but the web view was unalble to load - %@", error.localizedDescription]
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"OK"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                    }];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
