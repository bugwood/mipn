//
//  DetailRootViewController.h
//  ivegot1
//
//  Created by Jordan Daniel on 7/17/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailRootViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UILabel *navigationHint;


@end
