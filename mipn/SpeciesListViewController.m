//
//  SpeciesListViewController.m
//  GeneralFrameWork
//
//  Created by Jordan Daniel on 6/1/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import "SpeciesListViewController.h"
#import "AppDelegate.h"
#import "SpeciDetailTableController.h"
#import "PhotoViewController.h"
#import "Speci+CoreDataProperties.h"
#import "ImagePageViewController.h"



@interface SpeciesListViewController () <UISearchBarDelegate, UISearchResultsUpdating>

@property BOOL sortByCommon;
@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) NSMutableArray *searchList;

@end

@implementation SpeciesListViewController

@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize sortByCommon;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[[UIDevice currentDevice]systemVersion]floatValue]<7.0) {
        [[self.navigationController navigationBar]setBarStyle:UIBarStyleBlack];
    }
    
    sortByCommon = YES;
    if (self.selectedGroup != nil) {
        [self.navigationItem setTitle:[self.selectedGroup label]];
    }else{
        [self.navigationItem setTitle:@"All Species"];
    }
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.obscuresBackgroundDuringPresentation = NO;
    self.searchController.searchBar.scopeButtonTitles = @[@"Common Name",@"Scientific Name"];
    [self.searchController.searchBar sizeToFit];
    self.searchController.searchBar.delegate = self;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;
    
    NSError *error;
    if(![self.fetchedResultsController performFetch:&error]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:@"Sorry but there was an error gathering information."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"OK"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * _Nonnull action) {
                                                            [alert dismissViewControllerAnimated:YES completion:nil];
                                                        }];
        [alert addAction:dismiss];
        [self presentViewController:alert animated:YES completion:nil];
        exit(-1);
    }
    
    self.searchList = [NSMutableArray arrayWithCapacity:[[self.fetchedResultsController fetchedObjects]count]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changeSort:(id)sender {
    
    if (sortByCommon) {
        sortByCommon = NO;
        [self.changeSort setTitle:@"Common"];
    }else{
        [self.changeSort setTitle:@"Scientific"];
        sortByCommon = YES;
        
    }
    
    _fetchedResultsController.delegate=nil;
    _fetchedResultsController=nil;
    NSError *error;
    if(![self.fetchedResultsController performFetch:&error]){
        NSLog(@"Error perfroming fetch : %@", [error localizedDescription]);
    }
    [self.tableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ViewSpeciDetailsSegue"]) {
        if ([sender isKindOfClass:[NSManagedObject class]]) {
            UITabBarController *tabController = segue.destinationViewController;
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
            UIStoryboard *storyBoard = [appDelegate.window.rootViewController storyboard];
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
                tabController.hidesBottomBarWhenPushed = YES;
            }else{
                UINavigationController *nav = [[[appDelegate splitViewController]viewControllers]objectAtIndex:1];
                [nav popToRootViewControllerAnimated:NO];
            }
            
            SpeciDetailTableController *tableViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DetailsTab"];
            [tabController addChildViewController:tableViewController];
            tableViewController.selectedSpecies = sender;

            if ([[sender images]count]>0) {
                ImagePageViewController *pageViewController = [storyBoard instantiateViewControllerWithIdentifier:@"ImagesTab"];
                [pageViewController setDataSource:pageViewController];
                [pageViewController setIndex:0];
                [pageViewController setImageCount:[[sender images]count]];
                [pageViewController setViewControllers:@[[PhotoViewController photoViewControllerForPageIndex:0 selectedSpecies:sender]] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:NULL];
                [tabController addChildViewController:pageViewController];

            }
        }
    }
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([self.searchController isActive]) {
        return 1;
    } else {
        return [[self.fetchedResultsController sections]count];
        
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.searchController isActive]) {
        return [self.searchList count];
    } else {
        id  sectionInfo =[[self.fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if ([self.searchController isActive]) {
        return @"Search Results";
    } else {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo name];
    }
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    NSMutableArray *sectionTitles = [[NSMutableArray alloc] initWithCapacity:[[self.fetchedResultsController sections]count]];
    if (![self.searchController isActive] && self.selectedGroup == nil) {
        
        for (id <NSFetchedResultsSectionInfo> section in [self.fetchedResultsController sections]) {
            [sectionTitles addObject:[section name]];
        }
    }
    return sectionTitles;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString*)title atIndex:(NSInteger)index {
    return [self.fetchedResultsController sectionForSectionIndexTitle:title atIndex:index];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 88.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *aSpecies;
    if ([self.searchController isActive]) {
        aSpecies = [self.searchList objectAtIndex:indexPath.row];
    } else {
        aSpecies = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
    
    static NSString *CellIdenitifer;
    CellIdenitifer = @"SpeciCell";
        
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdenitifer];
    UILabel *textLabel = (UILabel *)[cell viewWithTag:1];
    UILabel *detailTextLabel = (UILabel*)[cell viewWithTag:2];
    
    if (sortByCommon) {
        textLabel.text = [aSpecies valueForKey:@"label"];
        [textLabel setFont:[UIFont boldSystemFontOfSize:17.0f]];
        detailTextLabel.text = [aSpecies valueForKey:@"sublabel"];
        [detailTextLabel setFont:[UIFont italicSystemFontOfSize:16.0f]];
    }else{
        textLabel.text = [aSpecies valueForKey:@"sublabel"];
        detailTextLabel.text = [aSpecies valueForKey:@"label"];
        [textLabel setFont:[UIFont fontWithName:@"Helvetica-BoldOblique" size:17.0f]];
        [detailTextLabel setFont:[UIFont systemFontOfSize:16.0f]];
    }
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:3];
    NSString *imageName = [aSpecies valueForKey:@"indicatorThumb"];
    NSString *filename = [[NSBundle mainBundle]pathForResource:[imageName substringToIndex:[imageName length]-4] ofType:[imageName substringFromIndex:[imageName length]-4]];
    [imageView setImage:[UIImage imageWithContentsOfFile:filename]];
    
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"object selected - %@",[self.searchController isActive]?[self.searchList objectAtIndex:indexPath.row]:[self.fetchedResultsController objectAtIndexPath:indexPath]);
    Speci *selectedSpeci;
    if (self.selectedGroup == nil) {
        NSString *speciesname;
        if ([self.searchController isActive]) {
            speciesname = [[self.searchList objectAtIndex:indexPath.row] valueForKey:@"label"];
        } else {
            speciesname = [[self.fetchedResultsController objectAtIndexPath:indexPath] valueForKey:@"label"];
        }
        NSFetchRequest *getDetails = [[NSFetchRequest alloc]init];
        [getDetails setEntity:[NSEntityDescription entityForName:@"Speci" inManagedObjectContext:[self.fetchedResultsController managedObjectContext]]];
        NSPredicate *speci = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"label == \"%@\"",speciesname]];
        [getDetails setPredicate:speci];
        NSError *err;
        selectedSpeci = (Speci*)[[[self.fetchedResultsController managedObjectContext] executeFetchRequest:getDetails error:&err]objectAtIndex:0];
        NSLog(@"%@,%@",[selectedSpeci label], [selectedSpeci sublabel]);
    }else{

        if ([self.searchController isActive]) {
            selectedSpeci = [self.searchList objectAtIndex:indexPath.row];
        } else {
            selectedSpeci = [self.fetchedResultsController objectAtIndexPath:indexPath];
        }
    }
    [self performSegueWithIdentifier:@"ViewSpeciDetailsSegue" sender:selectedSpeci];
}

#pragma mark - FetchedResultsController Property
/*
 Delegate for retrieving the user in the persistant store
 */
- (NSFetchedResultsController *)fetchedResultsController{
    if(_fetchedResultsController != nil){
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Speci" inManagedObjectContext:managedObjectContext];
    
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:20];
    
    NSArray *sortDescriptors;
    
    NSSortDescriptor *sections = [[NSSortDescriptor alloc]initWithKey:@"subgroup" ascending:YES];
    
    if (self.selectedGroup != nil) {
        if (sortByCommon) {
            NSSortDescriptor *sortByCom = [[NSSortDescriptor alloc]initWithKey:@"label" ascending:YES];
            NSSortDescriptor *sortByScientific = [[NSSortDescriptor alloc]initWithKey:@"sublabel" ascending:YES];
            sortDescriptors = @[sections, sortByCom, sortByScientific];
        }
        else{
            NSSortDescriptor *sortByScientific = [[NSSortDescriptor alloc]initWithKey:@"sublabel" ascending:YES];
            NSSortDescriptor *sortByCom = [[NSSortDescriptor alloc]initWithKey:@"label" ascending:YES];
            sortDescriptors = @[sections, sortByScientific, sortByCom];
        }
        
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        NSPredicate *groupSelected = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"group CONTAINS[cd] \'%@\'", [self.selectedGroup sublabel]]];
        [fetchRequest setPredicate:groupSelected];
        
        
        _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:@"subgroup" cacheName:nil];
        
        [_fetchedResultsController setDelegate:self];
        
    }else{
        NSString *sectionType;
        if (sortByCommon) {
            sectionType = @"sciIntial";
            NSSortDescriptor *sortByCom = [[NSSortDescriptor alloc]initWithKey:@"label" ascending:YES];
            NSSortDescriptor *sortByScientific = [[NSSortDescriptor alloc]initWithKey:@"sublabel" ascending:YES];
            
            sortDescriptors = @[sortByCom, sortByScientific];
        }
        else{
            sectionType = @"comIntial";
            NSSortDescriptor *sortByScientific = [[NSSortDescriptor alloc]initWithKey:@"sublabel" ascending:YES];
            NSSortDescriptor *sortByCom = [[NSSortDescriptor alloc]initWithKey:@"label" ascending:YES];
            sortDescriptors = @[sortByScientific, sortByCom];
        }
        
        [fetchRequest setSortDescriptors:sortDescriptors];
        [fetchRequest setReturnsDistinctResults:YES];
        [fetchRequest setResultType:NSDictionaryResultType];
//        [fetchRequest setPropertiesToFetch:@[@"label", @"sublabel",@"indicatorThumb",@"comIntial", @"sciIntial", @"searchText"]];
        
        _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:sectionType cacheName:nil];
        
        _fetchedResultsController.delegate = nil;
    }
    
    return _fetchedResultsController;
}

#pragma mark - NSFetchedResultsControllerDelegate Methods
-(void)controllerWillChangeContent:(NSFetchedResultsController *)controller{
    
}
-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
    
}
-(void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type{
    
}
-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath{
    
}

#pragma mark - SearchResultsUpdating delegate Methods
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = searchController.searchBar.text;
    [self searchForText:searchString scope:searchController.searchBar.selectedScopeButtonIndex];
    [self.tableView reloadData];
}

- (void)searchForText:(NSString *)searchText scope:(NSInteger)scopeOption{
    NSString *scope;
    switch (scopeOption) {
        case 0:
            scope = @"label";
            break;
        case 1:
            scope = @"sublabel";
            break;
            
        default:
            break;
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ contains[c] %@",scope,searchText];
    NSSortDescriptor *sortby = [NSSortDescriptor sortDescriptorWithKey:scope ascending:YES selector:@selector(caseInsensitiveCompare:)];
    NSLog(@"%@",predicate);
    
    self.searchList = [NSMutableArray arrayWithArray:[[self.fetchedResultsController.fetchedObjects sortedArrayUsingDescriptors:@[sortby]]filteredArrayUsingPredicate:predicate]];
//    NSLog(@"%@",self.searchList);
}

#pragma mark - SearchBar delegate Methods
- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope
{
    [self updateSearchResultsForSearchController:self.searchController];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    self.searchList = nil;
    [self.tableView reloadData];
}

@end
