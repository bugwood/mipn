//
//  main.m
//  mipn
//
//  Created by Jordan Daniel on 8/14/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
