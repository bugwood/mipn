//
//  GroupsViewController.h
//  mipn
//
//  Created by Jordan Daniel on 8/14/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupsViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@end
