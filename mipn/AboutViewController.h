//
//  AboutViewController.h
//  srsfieldguide
//
//  Created by Jordan Daniel on 6/26/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface AboutViewController : UIViewController
@property (weak, nonatomic) IBOutlet WKWebView *aboutView;

@end
