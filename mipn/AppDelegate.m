//
//  AppDelegate.m
//  mipn
//
//  Created by Jordan Daniel on 8/14/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import "AppDelegate.h"
#import "DetailRootViewController.h"
#import "Group+CoreDataProperties.h"
#import "Speci+CoreDataProperties.h"

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
        self.splitViewController = (UISplitViewController*)[storyboard instantiateViewControllerWithIdentifier:@"intialSplitScreen"];
        [self.splitViewController setDelegate:(DetailRootViewController*)[[[self.splitViewController viewControllers]objectAtIndex:1] topViewController]];
        self.window.rootViewController = self.splitViewController;
    }
    
    if (!([[[NSUserDefaults standardUserDefaults]objectForKey:@"DataVersion"]doubleValue]==[[[[[self managedObjectModel]versionIdentifiers]allObjects]objectAtIndex:0] doubleValue])) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath =  [documentsDirectory stringByAppendingPathComponent:@"mipn.sqlite"];
        
        if([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
            NSLog(@"file exsists and deleting it");
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
        }
        
        [[NSUserDefaults standardUserDefaults]setObject:[[[[self managedObjectModel]versionIdentifiers]allObjects]objectAtIndex:0] forKey:@"DataVersion"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"username"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"password"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"loggedIn"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
        return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

-(void)refreshDatabase{
    NSLog(@"refreshing Database");
    NSError *error;
    NSArray *groups = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"tagCategories" ofType:@"json"]]
                                                      options:NSJSONReadingAllowFragments error:&error];
    for (NSDictionary *group in groups) {
        Group *g = [NSEntityDescription insertNewObjectForEntityForName:@"Group" inManagedObjectContext:self.managedObjectContext];
        [g setCategory:[group objectForKey:@"category"]];
        [g setStandardImage:[group objectForKey:@"thumb"]];
        [g setLabel:[group objectForKey:@"label"]];
        [g setSublabel:[group objectForKey:@"sublabel"]];
        [g setDivision:[group objectForKey:@"division"]];
        [self.managedObjectContext save:&error];
    }
    
    NSArray *allSpecies = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"tags" ofType:@"json"]]
                                                          options:NSJSONReadingAllowFragments error:&error];
    for (NSDictionary *species in allSpecies) {
        Speci *s = [NSEntityDescription insertNewObjectForEntityForName:@"Speci" inManagedObjectContext:self.managedObjectContext];
        [s setSearchText:[species objectForKey:@"searchText"]];
        [s setSublabel:[species objectForKey:@"sublabel"]];
        [s setLabel:[species objectForKey:@"label"]];
        [s setGroup:[species objectForKey:@"tagCategory"]];
        [s setDescript:[species objectForKey:@"description"]];
        [s setImages:[species objectForKey:@"images"]];
        [s setIndicatorThumb:[species objectForKey:@"thumbnail"]];
        [s setComIntial:[[species objectForKey:@"sublabel"]substringToIndex:1]];
        [s setSciIntial:[[species objectForKey:@"label"]substringToIndex:1]];
        [s setSubgroup:[species objectForKey:@"status"]];
        
        [self.managedObjectContext save:&error];
        
    }
    
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"mipn" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"mipn.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error]) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath =  [documentsDirectory stringByAppendingPathComponent:@"mipn.sqlite"];
        
        if([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
            NSLog(@"file exsists and deleting it");
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
        }
        _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error]){
            NSLog(@"Unresolved error in persistant store coordinator: %@, %@", error, [error userInfo]);
            abort();
        }
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
