//
//  GroupsViewController.m
//  mipn
//
//  Created by Jordan Daniel on 8/14/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import "GroupsViewController.h"
#import "AppDelegate.h"
#import "Group+CoreDataProperties.h"
#import "SpeciesListViewController.h"

@interface GroupsViewController ()
@property (strong, nonatomic, readonly)NSFetchedResultsController *fetchedResultsController;
@end

@implementation GroupsViewController

@synthesize fetchedResultsController = _fetchedResultsController;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[[UIDevice currentDevice]systemVersion]floatValue]<7.0) {
        [[self.navigationController navigationBar]setBarStyle:UIBarStyleBlack];
    }

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    NSError *error;
    if(![self.fetchedResultsController performFetch:&error]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:@"Sorry but there was an error gathering information."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"OK"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * _Nonnull action) {
                                                            [alert dismissViewControllerAnimated:YES completion:nil];
                                                        }];
        [alert addAction:dismiss];
        [self presentViewController:alert animated:YES completion:nil];
        exit(-1);
    }
    if([[self.fetchedResultsController fetchedObjects]count]==0){
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
        [appDelegate refreshDatabase];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ViewGroupSegue"]) {
        if ([sender isKindOfClass:[NSManagedObject class]]) {
            SpeciesListViewController *viewSelectedSpecies = segue.destinationViewController;
            viewSelectedSpecies.selectedGroup = sender;
            
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections]count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self.fetchedResultsController sections]objectAtIndex:section] numberOfObjects];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [[(Group*)[[[[self.fetchedResultsController sections] objectAtIndex:section] objects] firstObject] division] capitalizedString];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"GroupCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    Group *g = [[[[self.fetchedResultsController sections] objectAtIndex:indexPath.section] objects] objectAtIndex:indexPath.row];
    
    UILabel *label = (UILabel*)[cell viewWithTag:1];
    UILabel *detailLabel = (UILabel*)[cell viewWithTag:2];
    UIImageView *imageView = (UIImageView*)[cell viewWithTag:3];
    
    [label setText:[g label]];
    [detailLabel setText:[g sublabel]];
    [imageView setImage:[UIImage imageNamed:[g standardImage]]];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *selectedSpecies = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"ViewGroupSegue" sender:selectedSpecies];
}

#pragma mark - FetchedResultsController Property
/*
 Delegate for retrieving the user in the persistant store
 */
- (NSFetchedResultsController *)fetchedResultsController{
    if(_fetchedResultsController != nil){
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Group" inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:20];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc]initWithKey:@"division" ascending:NO];
    NSSortDescriptor *sort2 = [[NSSortDescriptor alloc]initWithKey:@"label" ascending:YES];
    //NSString *sectionKey = @"label";
    
    NSArray *sortDecsriptors = @[sort,sort2];
    
    [fetchRequest setSortDescriptors:sortDecsriptors];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:@"division" cacheName:@"GroupsWoody"];
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}

#pragma mark - NSFetchedResultsControllerDelegate Methods
-(void)controllerWillChangeContent:(NSFetchedResultsController *)controller{
    
}
-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
    
}
-(void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type{
    
}
-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath{
    
}







@end
