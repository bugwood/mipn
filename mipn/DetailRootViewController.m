//
//  DetailRootViewController.m
//  ivegot1
//
//  Created by Jordan Daniel on 7/17/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import "DetailRootViewController.h"
#import <objc/message.h>

@interface DetailRootViewController ()

@end

@implementation DetailRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setupActionLabel];
}

-(void)setupActionLabel {
    [[UIDevice currentDevice]beginGeneratingDeviceOrientationNotifications];
    if (UIDeviceOrientationIsPortrait([[UIDevice currentDevice] orientation])) {
        [self.navigationHint setText:@"Swipe from the left to display navigation tabs"];
    }else{
        [self.navigationHint setText:@""];
    }
    [[UIDevice currentDevice]endGeneratingDeviceOrientationNotifications];
}

- (void)viewWillTransitionToSize:(CGSize)size
       withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    [self setupActionLabel];
}
    
#pragma mark - UISplitViewDelegate methods
-(BOOL)splitViewController:(UISplitViewController *)splitViewController collapseSecondaryViewController:(UIViewController *)secondaryViewController ontoPrimaryViewController:(UIViewController *)primaryViewController{
    [self setupActionLabel];
    return NO;
}
    
-(BOOL)splitViewController:(UISplitViewController *)splitViewController showDetailViewController:(UIViewController *)vc sender:(id)sender{
    [self setupActionLabel];
    return NO;
}

@end
