//
//  AppDelegate.h
//  mipn
//
//  Created by Jordan Daniel on 8/14/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic)UISplitViewController *splitViewController;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (void)refreshDatabase;

@end
