//
//  SpeciesListViewController.h
//  GeneralFrameWork
//
//  Created by Jordan Daniel on 6/1/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Group+CoreDataProperties.h"

@interface SpeciesListViewController : UITableViewController <NSFetchedResultsControllerDelegate, UISearchBarDelegate, UISearchDisplayDelegate>

@property(strong, nonatomic, readonly) NSFetchedResultsController *fetchedResultsController;
@property(strong, nonatomic) Group *selectedGroup;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *changeSort;
- (IBAction)changeSort:(id)sender;

@end
