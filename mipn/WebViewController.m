//
//  WebViewController.m
//  mrwc
//
//  Created by Jordan Daniel on 10/10/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()
@property (strong, nonatomic)UIActivityIndicatorView *activityIndicator;
@end

@implementation WebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
	self.activityIndicator.frame = CGRectMake(0, 0, 50, 50);
	self.activityIndicator.center = [self.webView center];
	self.activityIndicator.hidesWhenStopped = TRUE;
	self.activityIndicator.backgroundColor = [UIColor grayColor];
	[self.view addSubview:self.activityIndicator];
    
    [self.webView setNavigationDelegate:self];
    if (self.pdfType) {
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:self.pdfType ofType:@"pdf"]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        [self.webView loadRequest:request];
    }else if (self.aboutPage){
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"about" ofType:@"html"]]]];
            }else{
        NSURL *url = [NSURL URLWithString:@"https://apps.bugwood.org/mobile/index.cfm"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        [self.webView loadRequest:request];
    }
    
    

	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
-(void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation{
    [self.activityIndicator startAnimating];
}

-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    [self.activityIndicator stopAnimating];
}

-(void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Webview Error"
                                                                   message:[NSString stringWithFormat:@"Sorry but the web view was unalble to load - %@", error.localizedDescription]
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"OK"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                    }];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
}
@end
