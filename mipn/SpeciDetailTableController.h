//
//  SpeciDetailTableController.h
//  GeneralFrameWork
//
//  Created by Jordan Daniel on 6/7/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "Speci+CoreDataProperties.h"

@interface SpeciDetailTableController : UITableViewController <UITableViewDataSource, UITableViewDelegate, WKNavigationDelegate, NSFetchedResultsControllerDelegate>

@property(nonatomic)Speci *selectedSpecies;
@property (weak, nonatomic) IBOutlet UILabel *SciLabel;
@property (weak, nonatomic) IBOutlet UILabel *comLabel;
@property (weak, nonatomic) IBOutlet UITextView *otherSciNames;
@property (weak, nonatomic) IBOutlet UITextView *otherComNames;
@property (weak, nonatomic) IBOutlet WKWebView *descriptWebView;
@property (weak, nonatomic) IBOutlet UIImageView *imageSample;


@end
