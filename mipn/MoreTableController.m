//
//  MoreTableController.m
//  srsfieldguide
//
//  Created by Jordan Daniel on 6/26/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import "MoreTableController.h"
#import "AppDelegate.h"
#import <sys/utsname.h>

@interface MoreTableController ()
@end

@implementation MoreTableController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[[UIDevice currentDevice]systemVersion]floatValue]<7.0) {
        [[self.navigationController navigationBar]setBarStyle:UIBarStyleBlack];
    }
    
    [self.versionLabel setText:[NSString stringWithFormat:@"Version: %@",[[[NSBundle mainBundle]infoDictionary]objectForKey:@"CFBundleShortVersionString"]]];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        if ([indexPath row]==2) {
            [self actionEmailComposer];
            
        }
    }else if([indexPath row]<2){
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        UIStoryboard *storyBoard = [[[appDelegate window]rootViewController]storyboard];
        UINavigationController *nav = [[[appDelegate splitViewController] viewControllers]objectAtIndex:1];
        [nav popToRootViewControllerAnimated:NO];
        UITableViewCell *cell= (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        NSString *moreView = [[NSString alloc]initWithString:[[cell textLabel]text]];
        UIViewController *nextView = (UIViewController *)[storyBoard instantiateViewControllerWithIdentifier:moreView];
        [nav pushViewController:nextView animated:YES];
    }else if([indexPath row]==2){
        [self actionEmailComposer];
    }


}

- (IBAction)actionEmailComposer{
    
    if ([MFMailComposeViewController canSendMail]) {
        struct utsname systemInfo;
        uname(&systemInfo);
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        [mailViewController setMailComposeDelegate:self];
        [mailViewController setToRecipients:@[@"cbargero@uga.edu",@"tjdaniel@uga.edu"]];
        [mailViewController setSubject:@"Landscape Feedback"];
        NSMutableString *body = [[NSMutableString alloc]init];
        [body appendString:[[NSString alloc]initWithFormat:@"\n\n\nDevice: %@\n", [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding]]];
        [body appendString:[[NSString alloc]initWithFormat:@"iOS Version: %@\n",[[UIDevice currentDevice]systemVersion]]];
        [body appendString:[[NSString alloc]initWithString:[NSString stringWithFormat:@"App Version: %@",[[[NSBundle mainBundle]infoDictionary]objectForKey:@"CFBundleShortVersionString"]]]];
        [mailViewController setMessageBody:body isHTML:NO];
        [self presentViewController:mailViewController animated:YES completion:NULL];
    }
    else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Unable to Send a Message"
                                                                       message:@"Sorry but the app is up able to send email. Be sure you have an activate email account on in your settings and try again"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"OK"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * _Nonnull action) {
                                                            [alert dismissViewControllerAnimated:YES completion:nil];
                                                        }];
        [alert addAction:dismiss];
        [self presentViewController:alert animated:YES completion:nil];
        NSLog(@"Device is unable to send email in its current state.");
    }
    
}

#pragma mark - Mail Compose View delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}


@end
