//
//  WebViewController.h
//  mrwc
//
//  Created by Jordan Daniel on 10/10/13.
//  Copyright (c) 2013 com.bugwood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface WebViewController : UIViewController <WKNavigationDelegate>
@property (strong, nonatomic) IBOutlet WKWebView *webView;
@property (strong, nonatomic) NSString *pdfType;
@property (strong, nonatomic) NSString *aboutPage;


@end
